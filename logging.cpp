#include "logging.h"
#include <iostream>
using std::to_string;

logging::Logger::~Logger() {
  m_file << flush;
  m_file.close();
}

logging::Logger::Logger(const std::string &fname_n) : _logLevel(2) {
  m_file.open(fname_n);

  if (!m_file.is_open())
    throw std::runtime_error("Log file could not be opened!");
}

logging::Logger& logging::Logger::getLogger(){
    static Logger mainLogger("loginfo_DungeonCrawler");
    return mainLogger;
}

void logging::Logger::writeLog(int inf_n){
    int logLevel = 0;
    string info = " ";

    switch(inf_n){
    case(0):{
        info = "Game started";
        logLevel = 2;
        break;
    }
    case(1):{
        info = "Game ended";
        logLevel = 2;
        break;
    }
    case(2):{
        info = "False direction";
        logLevel = 1;
        break;
    }
    case(3):{
        info = "Unknown error";
        logLevel = 0;
        break;
    case(4):{
        info = "Game won";
        logLevel = 2;
        break;
    }
    }
    }

    getLogger().log(info, logLevel);
}

void logging::Logger::writeLog(char input_n){
    string logInfo = "Player's input: ";
    logInfo.push_back(input_n);
    getLogger().log(logInfo,2);
}

void logging::Logger::writeLog(int row_n, int col_n){
    getLogger().log("Figure is moved to tile ["+to_string(row_n)+"]["+to_string(col_n)+"]",2);
}

void logging::Logger::writeLog(int row_n, int col_n, int row2_n, int col2_n, int i_n){ // last parameter to differ between stepping on/ off
    if(i_n == 0){
        getLogger().log("Figure is moved from [" + to_string(row_n) + "][" + to_string(col_n) + "] to ["+to_string(row2_n)+"]["+to_string(col2_n)+"]",2);
    }
    else if(i_n == 1){
        getLogger().log("Figure is moved to [" + to_string(row2_n) + "][" + to_string(col2_n) + "] from ["+to_string(row_n)+"]["+to_string(col_n)+"]",2);
    }
}

void logging::Logger::writeLog(char c_n, int row_n, int col_n){
    if(c_n == 's'){
        getLogger().log("Switch [" + to_string(row_n) + "][" + to_string(col_n) + "] was activated", 2);
    }
    else if(c_n == 'd'){
        getLogger().log("Door [" + to_string(row_n) + "][" + to_string(col_n) + "] was opened", 2);

    }
}
