#include "Tiles/floor.h"

Floor::Floor(int row_n, int col_n) : Tile(row_n, col_n), _floorSign('.')
{

}

Tile* Floor::onEnter(Tile* fromTile_n){
    return fromTile_n;
}


char Floor::getIcon(){
    //if(getItem() != nullptr){ return '*'; }
    return _floorSign;
}
