#include "Tiles/portal.h"

Portal::Portal(int row_n, int col_n) : Tile(row_n, col_n), _portalSign('O')
{

}

Portal::~Portal(){
}

char Portal::getIcon(){
    //if(getItem() != nullptr){ return '*'; }
    return _portalSign;
}

//Tile* Portal::onLeave(Tile* destTile_n){
//    return destTile_n;
//}

Tile* Portal::onEnter(Tile *fromTile_n){
    return _destination;
}

void Portal::setDestination(Portal* destination_n){
    _destination = destination_n;
}

