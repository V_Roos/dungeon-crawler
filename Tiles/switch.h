#ifndef SWITCH_H
#define SWITCH_H
#include "Tiles/active.h"
#include "Tiles/floor.h"


class Switch: public Active, public Floor
{
public:
    Switch(int row_n, int col_n);

    virtual char getIcon() override;
    virtual Tile* onEnter(Tile *fromTile_n) override;
    virtual void activate() override;

private:
    char _switchSign;
};

#endif // SWITCH_H
