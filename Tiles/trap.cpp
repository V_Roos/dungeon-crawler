#include "Tiles/trap.h"
#include "logging.h"
#include "character.h"


Trap::Trap(int row_n, int col_n) : Floor(row_n, col_n), _trapSign(Floor::getIcon())
{

}

char Trap::getIcon(){
    //if(getItem() != nullptr){ return '*'; }
    return _trapSign;
}

Tile* Trap::onLeave(Tile *destTile_n){
    if(this->getCharacter() != nullptr){
        if(this->getIcon() == Floor::getIcon()){
            this->getCharacter()->setHP(-20);
            _trapSign = 'T';
        }
    }
    return this;
}
