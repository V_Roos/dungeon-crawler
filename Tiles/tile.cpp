#include "Tiles/tile.h"
#include "character.h"
#include "logging.h"

Tile::Tile(const int row_n, const int col_n) : _row(row_n), _column(col_n)
{

}


Tile::~Tile(){
    delete _character;
}

//getter-setter
//Item* Tile::getItem(){
//    return _item;
//}

Character* Tile::getCharacter(){
    return _character;
}

//void Tile::setItem(Item *item_n){
//    _item = item_n;
//}

void Tile::setCharacter(Character *newCharacter_n){
    _character=newCharacter_n;
}

int Tile::getRow() const{
    return _row;
}

int Tile::getColumn() const{
    return _column;
}

bool Tile::hasCharacter(){
    if(getCharacter()!=nullptr) { return true; }
    return false;
}


bool Tile::moveTo(Tile* destTile_n){        // same function in trap.cpp
    Tile* leftTile=onLeave(destTile_n);
    destTile_n = destTile_n->onEnter(destTile_n);

    if(destTile_n == nullptr || leftTile == nullptr){
        logging::Logger::getLogger().writeLog(3);
        return false;
    }

    if(destTile_n->getCharacter() != nullptr){
        //logging
        return false;    }

    logging::Logger::getLogger().writeLog(leftTile->getRow(), leftTile->getColumn(), destTile_n->getRow(), destTile_n->getColumn(), 0);


    destTile_n->setCharacter(leftTile->getCharacter()); //character aktualisieren
    destTile_n->getCharacter()->setTile(destTile_n);      //tile aktualisieren

//    if(destTile_n->getItem() != nullptr){
//        destTile_n->getItem()->onEquip(destTile_n->getCharacter());
//        destTile_n->setItem(nullptr);
//    }

    logging::Logger::getLogger().writeLog(destTile_n->getRow(), destTile_n->getColumn(), leftTile->getRow(), leftTile->getColumn(), 1);
    leftTile->setCharacter(nullptr);

    return true;
}

Tile* Tile::onEnter(Tile *fromTile_n){
    return fromTile_n;
}

Tile* Tile::onLeave(Tile *destTile_n){
    return this;
}
