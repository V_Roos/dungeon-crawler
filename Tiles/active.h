#ifndef ACTIVE_H
#define ACTIVE_H
#include "Tiles/passive.h"
#include <vector>
using std::vector;


class Active
{
public:
    Active();

    void attach(Passive* passive_n);
    void detach(Passive* passive_n);
    virtual void activate() = 0; //todo: change function definition

protected:
    vector<Passive*> _passiveObjects;

};

#endif // ACTIVE_H
