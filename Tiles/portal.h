#ifndef PORTAL_H
#define PORTAL_H
#include "Tiles/tile.h"


class Portal : public Tile
{
public:
    Portal(int row_n, int col_n);
    virtual ~Portal();

    virtual char getIcon() override;
//    virtual Tile* onLeave(Tile* destTile_n) override;
    virtual Tile* onEnter(Tile *fromTile_n) override;
    void setDestination(Portal* destination_n);


private:
    char _portalSign;
    Portal* _destination;
};

#endif // PORTAL_H
