#include "Tiles/door.h"
#include "logging.h"

Door::Door(int row_n, int col_n) : Tile(row_n, col_n), _doorSign('X')
{

}

char Door::getIcon(){
    //if(getItem() != nullptr){ return '*'; }
    return _doorSign;
}

Tile* Door::onEnter(Tile *fromTile_n){
    if(_doorSign == 'X'){
        return nullptr;
    }
    else{
        return fromTile_n;
    }
}

void Door::notify(){
    if(_doorSign == 'X'){
    _doorSign = '/';
    logging::Logger::getLogger().writeLog('d', this->getRow(), this->getColumn());
    }
    else if(_doorSign == '/'){
        _doorSign = 'X';
        //logging
    }
}
