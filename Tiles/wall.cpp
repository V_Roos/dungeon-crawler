#include "Tiles/wall.h"

Wall::Wall(int row_n, int col_n) : Tile(row_n, col_n), _wallSign('#')
{

}

char Wall::getIcon(){
    //if(getItem() != nullptr){ return '*'; }
    return _wallSign;
}

Tile* Wall::onEnter(Tile *fromTile_n){
    return nullptr;
}
