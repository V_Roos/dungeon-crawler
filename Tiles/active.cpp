#include "Tiles/active.h"
#include <algorithm>

Active::Active()
{

}

void Active::attach(Passive* passive_n){
    _passiveObjects.push_back(passive_n);
}

void Active::detach(Passive* passive_n){
    _passiveObjects.erase(remove(_passiveObjects.begin(), _passiveObjects.end(), passive_n), _passiveObjects.end());
}

//void Active::activate(function<void(Passive*)> function_n){
//    for(Passive* &pObj : _passiveObjects){
//        pObj->notify(function_n);
//    }
//}
