#ifndef TILE_H
#define TILE_H
#include <iostream>
#include "Items/item.h"
using std::string;

class Character;

class Tile
{
public:
    Tile(const int row_n, const int col_n);
    virtual ~Tile();

    //getter-setter
    //Item* getItem();
    Character* getCharacter();
    //void setItem(Item* item_n);
    void setCharacter(Character* newCharacter_n);
    int getRow() const;
    int getColumn() const;
    virtual char getIcon() = 0;

    bool hasCharacter();

    bool moveTo(Tile* destTile_n);
    virtual Tile* onEnter(Tile* fromTile_n);
    virtual Tile* onLeave(Tile* destTile_n);

private:
    Character* _character = nullptr;
    const int _row;
    const int _column;
    //Item* _item = nullptr;
};

#endif // TILE_H
