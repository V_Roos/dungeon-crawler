#ifndef DOOR_H
#define DOOR_H
#include "Tiles/passive.h"
#include "Tiles/tile.h"


class Door : public Passive, public Tile
{
public:
    Door(int row_n, int col_n);

    virtual char getIcon() override;
    virtual Tile* onEnter(Tile *fromTile_n) override;
    virtual void notify() override;   // open door here

private:
    char _doorSign;
};

#endif // DOOR_H
