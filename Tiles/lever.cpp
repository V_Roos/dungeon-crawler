#include "Tiles/lever.h"

Lever::Lever(int row_n, int col_n) : Switch(row_n, col_n), _leverSign('L')
{

}

char Lever::getIcon(){
    //if(getItem() != nullptr){ return '*'; }
    return _leverSign;
}

Tile* Lever::onEnter(Tile *fromTile_n){
    activate();
    //logging
    return fromTile_n;
}

void Lever::activate(){
    for(Passive* &P : _passiveObjects){
        P->notify();
    }
}
