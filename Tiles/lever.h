#ifndef LEVER_H
#define LEVER_H
#include "Tiles/switch.h"


class Lever: public Switch
{
public:
    Lever(int row_n, int col_n);

    virtual char getIcon() override;
    virtual Tile* onEnter(Tile *fromTile_n) override;
    virtual void activate() override;

private:
    char _leverSign;
};

#endif // LEVER_H
