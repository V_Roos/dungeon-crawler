#ifndef TRAP_H
#define TRAP_H
#include "Tiles/floor.h"



class Trap: public Floor
{
public:
    Trap(int row_n, int col_n);

    virtual char getIcon() override;
    virtual Tile* onLeave(Tile *destTile_n) override;

private:
    char _trapSign;
};

#endif // TRAP_H
