#ifndef PASSIVE_H
#define PASSIVE_H


class Passive
{
public:
    Passive();
    virtual void notify() = 0;   // todo: change parameter
};

#endif // PASSIVE_H
