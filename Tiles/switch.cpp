#include "Tiles/switch.h"
#include "logging.h"

Switch::Switch(int row_n, int col_n) : Floor(row_n, col_n), _switchSign('?')
{

}


char Switch::getIcon(){
    //if(getItem() != nullptr){ return '*'; }
    return _switchSign;
}

Tile* Switch::onEnter(Tile *fromTile_n){
    if(_switchSign == '?'){
        _switchSign = '!';
        activate();
        logging::Logger::getLogger().writeLog('s', this->getRow(), this->getColumn());
    }
    return fromTile_n;
}


void Switch::activate(){
    for(Passive* &P : _passiveObjects){
        P->notify();
    }
}
