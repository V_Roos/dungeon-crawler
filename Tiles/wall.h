#ifndef WALL_H
#define WALL_H
#include "Tiles/tile.h"


class Wall: public Tile
{
public:
    Wall(int row_n, int col_n);

    virtual char getIcon() override;
    virtual Tile* onEnter(Tile *fromTile_n) override;

private:
    char _wallSign;
};

#endif // WALL_H
