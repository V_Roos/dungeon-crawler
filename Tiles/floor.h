#ifndef FLOOR_H
#define FLOOR_H
#include "Tiles/tile.h"


class Floor : public Tile
{
public:
    Floor(int row_n, int col_n);

    virtual Tile* onEnter(Tile* fromTile_n) override;
    virtual char getIcon() override;

private:
    char _floorSign;
};

#endif // FLOOR_H
