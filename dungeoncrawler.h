#ifndef DUNGEON_CRAWLER_H
#define DUNGEON_CRAWLER_H
#include <vector>
#include "Controllers/userinterface.h"
#include "level.h"
#include "Tiles/tile.h"
#include "character.h"
using std::vector;

class DungeonCrawler
{
public:
    DungeonCrawler(UserInterface* interface_n, Level* level_n);


    void play();
    bool menuManager(); // verwaltet Prozesse, wenn Menü aufgerufen wird
    void manipulateItem(int input); // findet das richtige Item und ruft onDrop() aus item.cpp

    bool moveFigures(char go);
    Tile *determineDestTile(const int go_n, Tile* tile_n);

private:
    UserInterface* _userInterface;
    Level* _level;
    vector<Character*> _allFigures;
};

#endif // DUNGEON_CRAWLER_H
