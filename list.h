#ifndef LIST_H
#define LIST_H
#include <iterator>
#include "Items/item.h"


class List {
    using size_t = unsigned long;

private:
  struct Element {
    //Item* data;
    Element *next = nullptr;
    Element *prev = nullptr;
  };

  //static Element *createElement(const Item &data);

  Element *m_first = nullptr;
  Element *m_last = nullptr;

  size_t m_size = 0;


public:
  //using size_t = unsigned long;

  List();
  ~List();

  bool empty() const;

//  void push_back(const Item &value);
//  void push_front(const Item &value);

  void pop_back();
  void pop_front();

//  const Item &front() const;
//  const Item &back() const;

//  Item &front();
//  Item &back();

  void clear();

  size_t size() const;



//  void remove(Item* item_n);

  class iterator{
  public:
      iterator(Element* element_n);

      friend class List;

      iterator& operator++();
      iterator& operator--();
      //Item*& operator*();
      bool operator==(const iterator& it_n);
      bool operator!=(const iterator& it_n);

  private:
      Element* _currentElement;
  };

  iterator begin();
  iterator end();
};

#endif // LIST_H
