#ifndef LOGGING_H
#define LOGGING_H
#include <fstream>
#include <string>
using std::ofstream;
using std::string;

#include <chrono>
#include <ctime>
#include <iomanip>
#include <iostream>
using std::flush;
using std::localtime;
using std::put_time;
using std::chrono::system_clock;


namespace logging {
class Logger {
public:
  Logger(const Logger&) = delete;
  ~Logger();

  static Logger& getLogger();


  template <typename T> void log(const T &info, unsigned int level) {

    auto time = system_clock::to_time_t(system_clock::now());
    _logLevel = level;

    if(level <=2) {
        string level_name = "Info";

        if(_logLevel == 1){
            level_name = "Warn";
        }
        else if(_logLevel == 0){
            level_name = "Error";
        }

    m_file << level_name << std::put_time(std::localtime(&time), "[%F %T] ") << info
    << std::endl;
    }
  }

void writeLog(int inf_n);
void writeLog(char input_n);
void writeLog(int row_n, int col_n);
void writeLog(int row_n, int col_n, int row2_n, int col2_n, int i_n);
void writeLog(char c_n, int row_n, int col_n);


private:
  Logger(const string &fname_n);
  ofstream m_file;
  unsigned int _logLevel;
};
} // namespace logging
#endif // LOGGING_H
