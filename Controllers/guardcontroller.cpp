#include "Controllers/guardcontroller.h"

GuardController::GuardController(string movement_n) : _counter(0)
{
    for(size_t i = 0; i < movement_n.size(); i++){
        _movement.push_back(movement_n[i]);
    }
}


char GuardController::move(){
    if(_counter == _movement.size()){
        _counter = 0;
    }

    return _movement.at(_counter++);
}
