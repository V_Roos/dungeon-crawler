#include "Controllers/userinterface.h"
#include "character.h"
#include "list.h"
#include <iostream>
using std::to_string;

UserInterface::UserInterface() : Controller()
{
    initscr();
    noecho();
    cbreak();
    curs_set(0);
    _characterWindow = newwin(100,100,0, 0);
    _levelWindow = newwin(100, 100, 8, 0);
}

UserInterface::~UserInterface()
{
    wclear(_characterWindow);
    wrefresh(_characterWindow);
    wclear(_levelWindow);
    wrefresh(_levelWindow);
    delwin(_characterWindow);
    delwin(_levelWindow);
    endwin();
}

void UserInterface::draw(Level *level_n){
    wclear(_characterWindow);
    wclear(_levelWindow);

    Character* actualPlayer = getActualPlayer(level_n);
    string info;

    info = "Actual player: " + to_string(actualPlayer->getIcon());
    waddstr(_characterWindow, info.c_str());                            waddstr(_characterWindow, "\n");

    info = "Strength: " + to_string(actualPlayer->getStrength());
    waddstr(_characterWindow, info.c_str());                            waddstr(_characterWindow, "\n");

    info = "Stamina: " + to_string((actualPlayer->getStamina()));
    waddstr(_characterWindow, info.c_str());                            waddstr(_characterWindow, "\n");

    info = "Hitpoints: " + to_string(actualPlayer->getHP());
    waddstr(_characterWindow, info.c_str());                            waddstr(_characterWindow, "\n");

//    _levelWindow->_maxx = level_n->getRows()+1;
//    _levelWindow->_maxy = level_n->getCols();

    for(int i=0; i<level_n->getRows(); i++){
        for(int j=0; j<level_n->getCols(); j++){
            if(level_n->getTile(i,j)->getCharacter()!=nullptr){
                waddch(_levelWindow, level_n->getTile(i,j)->getCharacter()->getIcon());
                waddch(_levelWindow, ' ');
            }
            else{
                waddch(_levelWindow, level_n->getTile(i,j)->getIcon());
                waddch(_levelWindow, ' ');
            }
        }
        waddstr(_levelWindow, "\n");
    }
    waddstr(_levelWindow, "Press Esc to open menu");
    wrefresh(_characterWindow);
    wrefresh(_levelWindow);
}


void UserInterface::drawMenu(){
    wclear(_levelWindow);
    waddstr(_levelWindow, "Press 1 to show inventory");     waddstr(_levelWindow, "\n");
    waddstr(_levelWindow, "Press 2 to end this game");      waddstr(_levelWindow, "\n");
    waddstr(_levelWindow, "Press any key to continue");     waddstr(_levelWindow, "\n");
    wrefresh(_levelWindow);
}


char UserInterface::move(){
    char direction=0;
    direction = wgetch(_levelWindow);
    return direction;
}


Character* UserInterface::getActualPlayer(Level* level_n){
    for(int i=0; i<level_n->getRows(); i++){
        for(int j=0; j<level_n->getCols(); j++){
            if(level_n->getTile(i,j)->getCharacter() != nullptr){
                if(level_n->getTile(i,j)->getCharacter()->getIcon() == '@'){            // attantion
                    return level_n->getTile(i,j)->getCharacter();
                }
            }
        }
    }
    return nullptr;
}


void UserInterface::printInventory(Level *level_n){
    wclear(_levelWindow);
    Character* actualPlayer = getActualPlayer(level_n);
    int c = 1;

//    for(List::iterator i = actualPlayer->getAllItems().begin(); i != actualPlayer->getAllItems().end(); ++i){
//        waddstr(_levelWindow, "Choose an item and press enter"); waddstr(_levelWindow, "/n");
//        waddch(_levelWindow, c);   waddch(_levelWindow, ' ');
//        //waddstr(_levelWindow, i.operator*()->getName().c_str());
//        waddstr(_levelWindow, "    ");
//        c++;
//    }
    wrefresh(_levelWindow);
}
