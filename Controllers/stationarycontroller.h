#ifndef STATIONARYCONTROLLER_H
#define STATIONARYCONTROLLER_H
#include "Controllers/controller.h"


class StationaryController : public Controller
{
public:
    StationaryController();

    virtual char move() override;
};

#endif // STATIONARYCONTROLLER_H
