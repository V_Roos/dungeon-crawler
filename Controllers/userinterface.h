#ifndef USER_INTERFACE_H
#define USER_INTERFACE_H
#include <ncurses.h>
#include "level.h"
#include "Controllers/controller.h"


class UserInterface : public Controller
{
public:
    UserInterface();
    virtual ~UserInterface();

    void draw(Level* level_n);
    virtual char move() override;
    void drawMenu();
    void printInventory(Level* level_n);

    Character* getActualPlayer(Level* level_n);

private:
    WINDOW* _characterWindow;
    WINDOW* _levelWindow;       // alle Eingaben nur hier mit wgetch
};

#endif // USER_INTERFACE_H
