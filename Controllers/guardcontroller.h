#ifndef GUARDCONTROLLER_H
#define GUARDCONTROLLER_H
#include "Controllers/controller.h"
#include <iostream>
#include <vector>
using std::vector;
using std::string;


class GuardController : public Controller
{
public:
    GuardController(string movement_n);

    virtual char move() override;

private:
    vector<char> _movement;
    size_t _counter;
};

#endif // GUARDCONTROLLER_H
