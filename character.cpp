#include "character.h"
#include "logging.h"
#include <ncurses.h>

Character::Character(const char ch_n, int sterength_n, int stamina_n) : _figure(ch_n), _strength(sterength_n), _stamina(stamina_n)
{
    _hitpoints = getMaxHP();
}


char Character::getIcon(){
    return _figure;
}

void Character::setTile(Tile* tile_n){
    _tile = tile_n;
}

Tile* Character::getTile(){
    return _tile;
}

char Character::move(){
    if(_controller != nullptr){
    return _controller->move();
    }
}

void Character::setController(Controller* control_n){
    _controller = control_n;
}

Controller* Character::getController(){
    return _controller;
}

void Character::setHP(int hp_n){
    _hitpoints = _hitpoints + hp_n;
}

int Character::getHP(){
    return _hitpoints;
}

int Character::getMaxHP(){
    _hitpoints = 20 + (_stamina*5);
    return _hitpoints;
}

int Character::getStrength(){
    return _strength;
}

int Character::getStamina(){
    return _stamina;
}

void Character::setStrength(int strength_n){
    _strength = _strength + strength_n;
}

void Character::setStamina(int stamina_n){
    _stamina = _stamina + stamina_n;
    _hitpoints = getMaxHP();
}

//void Character::addItem(Item *item_n){
//    //_allItems.push_back(*item_n);
//}

//void Character::removeItem(Item* item_n){
//    //_allItems.remove(item_n);
//}

List& Character::getAllItems(){
//    return _allItems;
}
