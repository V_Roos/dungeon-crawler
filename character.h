#ifndef CHARACTER_H
#define CHARACTER_H
#include "Controllers/controller.h" // forward
#include "list.h"
//#include "Items/item.h"
#include "Tiles/tile.h"

//class Item;
//class Tile;

class Character
{
public:
    Character(const char ch_n, int sterength_n, int stamina_n);
    //virtual ~Character();

    char getIcon();
    void setTile(Tile* tile_n);
    virtual char move();
    void setController(Controller* control_n);
    Controller* getController();

    Tile* getTile();


    int getHP();
    void setHP(int hp_n);
    int getStrength();
    int getStamina();
    void setStrength(int strength_n);
    void setStamina(int stamina_n);
//    void addItem(Item* item_n);
//    void removeItem(Item* item_n);
    List& getAllItems();


private:
    char _figure;
    Tile* _tile = nullptr;
    Controller* _controller = nullptr;

    int _strength;
    int _stamina;
    int _hitpoints;
//    List _allItems;

    int getMaxHP();
};

#endif // CHARACTER_H
