#include "dungeoncrawler.h"
#include "logging.h"
#include <ncurses.h>
#include <iostream>
using std::cin;

DungeonCrawler::DungeonCrawler(UserInterface* interface_n, Level* level_n) : _userInterface(interface_n), _level(level_n)
{
    for(int i=0; i<_level->getRows(); i++){
        for(int j=0; j<_level->getCols(); j++){
            if(_level->getTile(i,j)->getCharacter() != nullptr){
                if(_level->getTile(i,j)->getCharacter()->getIcon() == '@'){             // attention
                    _level->getTile(i,j)->getCharacter()->setController(interface_n);
                }
                _allFigures.push_back(_level->getTile(i,j)->getCharacter());                
            }
        }
    }
}

void DungeonCrawler::play(){
    logging::Logger::getLogger().writeLog(0);

    while(true){
        if(_allFigures.size() == 0) { break; }

        _userInterface->draw(_level);

        char go = _userInterface->move();
            logging::Logger::getLogger().writeLog(go);

        if(go==27){                         // Esc
            if(!menuManager()){  break;   }
            else { continue;  }
        }
        moveFigures(go);
    }
    logging::Logger::getLogger().writeLog(1);
}


bool DungeonCrawler::moveFigures(char go){
    Tile* nextTile = nullptr;

    for(size_t i = 0; i < _allFigures.size(); i++){
        if(determineDestTile(go, _allFigures.at(i)->getTile())==nullptr || (go!='q' && go!='w' && go!='e' && go!='a' && go!='s' && go!='d' && go!='y' && go!='x')){
            //logging
            return false;
        }
    }

    for(size_t i = 0; i < _allFigures.size(); i++){
        if(_allFigures.at(i)->getIcon() == '@'){         // attention
            nextTile = determineDestTile(go, _allFigures.at(i)->getTile());
            _allFigures.at(i)->getTile()->moveTo(nextTile);
        }
         else{
            char goFeind = _allFigures.at(i)->getController()->move();
            nextTile = determineDestTile(goFeind, _allFigures.at(i)->getTile());
            _allFigures.at(i)->getTile()->moveTo(nextTile);
        }
    }
    return true;
}


Tile* DungeonCrawler::determineDestTile(const int go_n, Tile* tile_n){
    int currentRow = tile_n->getRow();
    int currentCol = tile_n->getColumn();

    switch(go_n){
    case 'q':{
        currentRow-=1;
        currentCol-=1;
        break;}
    case 'w':{
        currentRow-=1;
        break;}
    case 'e':{
        currentRow-=1;
        currentCol+=1;
        break;}
    case 'a':{
        currentCol-=1;
        break;}
    case 's':{
        currentRow+=1;
        break;}
    case 'd':{
        currentCol+=1;
        break;}
    case 'y':{
        currentRow+=1;
        currentCol-=1;
        break;}
    case 'x':{
        currentRow+=1;
        currentCol+=1;
        break;}
    }
    if(currentRow<0 || currentRow>=_level->getRows() || currentCol<0 || currentCol>=_level->getCols()){
        return nullptr;
    }
    return _level->getTile(currentRow,currentCol);
}


bool DungeonCrawler::menuManager(){
    _userInterface->drawMenu();
    char go = _userInterface->move();

    if(go == '2') { return false;  }
    if(go == '1') {
        _userInterface->printInventory(_level);
        int input = 0;
        cin >> input;           // funktioniert?
        manipulateItem(input);
    }
    return true;
}

void DungeonCrawler::manipulateItem(int input){
    for (Character* &C : _allFigures) {
        if(C->getIcon() == '@'){                // attantion
            int x = 1; // counter

//            for(List::iterator i = C->getAllItems().begin(); i != C->getAllItems().end(); ++i){
//                if(input == x){
//                    //i.operator*()->onDrop(C, C->getTile());
//                }
//                ++x;
//            }
        }
    }
}
