#include <iostream>
#include "dungeoncrawler.h"
#include "Controllers/userinterface.h"
#include "level.h"

#include <vector>
#include <algorithm>

using namespace std;

int main()
{
    UserInterface _interface;
    Level _lev("/home/stud/Projects/P4/4.map");
    DungeonCrawler _dun(&_interface, &_lev);
    _dun.play();

    return 0;
}
