#ifndef LEVEL_H
#define LEVEL_H
#include <array>
#include "Tiles/tile.h"
#include <fstream>
using std::ifstream;

class Level
{
public:
    Level();
    Level(const string &level_n);
    Level(const Level &level_n) = delete;
    ~Level();

    int getRows();
    int getCols();

    Tile* getTile(int row_n, int col_n);
    const Tile* getTile(int row_n, int col_n) const;
    void placeCharacter(Character* c_n, int row_n, int col_n);

private:
    int _rows;
    int _columns;
    Tile*** _currentLevel;
};

#endif // LEVEL_H
