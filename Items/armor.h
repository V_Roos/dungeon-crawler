#ifndef ARMOR_H
#define ARMOR_H
#include "Items/item.h"


class Armor : public Item
{
public:
    Armor(const string& name_n, int staBonus_n);
    virtual ~Armor();

    virtual string getName() const override;
    virtual void onDrop(Character* character_n, Tile* tile_n) override;

private:
    int _staminaBonus;
};

#endif // ARMOR_H
