#include "Items/armor.h"
#include "character.h"

Armor::Armor(const string& name_n, int staBonus_n) : Item(name_n), _staminaBonus(staBonus_n)
{

}

string Armor::getName() const{
    return _name;
}

void Armor::onDrop(Character *character_n, Tile *tile_n){
    character_n->setStamina(-_staminaBonus);
    Item::onDrop(character_n, tile_n);
}
