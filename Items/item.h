#ifndef ITEM_H
#define ITEM_H
#include <iostream>
//#include "character.h"
using std::string;

class Character;
class Tile;

class Item
{
public:
    Item(const string& name_n);
    virtual ~Item();

    virtual void onEquip(Character* character_n);
    virtual void onDrop(Character* character_n, Tile* tile_n);
    virtual string getName() const = 0;

protected:
    string _name;
};

#endif // ITEM_H
