TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        Controllers/controller.cpp \
        Controllers/guardcontroller.cpp \
        Controllers/stationarycontroller.cpp \
        Controllers/userinterface.cpp \
        Items/armor.cpp \
        Items/consumable.cpp \
        Items/elixir.cpp \
        Items/item.cpp \
        Items/potion.cpp \
        Items/weapon.cpp \
        Tiles/active.cpp \
        Tiles/door.cpp \
        Tiles/floor.cpp \
        Tiles/lever.cpp \
        Tiles/passive.cpp \
        Tiles/portal.cpp \
        Tiles/switch.cpp \
        Tiles/tile.cpp \
        Tiles/trap.cpp \
        Tiles/wall.cpp \
        character.cpp \
        dungeoncrawler.cpp \
        level.cpp \
        list.cpp \
        logging.cpp \
        main.cpp \
        node.cpp \

HEADERS += \
    Controllers/controller.h \
    Controllers/guardcontroller.h \
    Controllers/stationarycontroller.h \
    Controllers/userinterface.h \
    Items/armor.h \
    Items/consumable.h \
    Items/elixir.h \
    Items/item.h \
    Items/potion.h \
    Items/weapon.h \
    Tiles/active.h \
    Tiles/door.h \
    Tiles/floor.h \
    Tiles/lever.h \
    Tiles/passive.h \
    Tiles/portal.h \
    Tiles/switch.h \
    Tiles/tile.h \
    Tiles/trap.h \
    Tiles/wall.h \
    character.h \
    dungeoncrawler.h \
    level.h \
    list.h \
    logging.h \
    node.h \

unix|win32: LIBS += -lncurses

DISTFILES += \
