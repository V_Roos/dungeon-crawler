#include "level.h"
#include "character.h"
#include <array>
#include <ios>
#include "node.h"
using std::ios;

#include "Tiles/portal.h"
#include "Tiles/wall.h"
#include "Tiles/floor.h"
#include "Tiles/door.h"
#include "Tiles/switch.h"
#include "Tiles/lever.h"
#include "Tiles/trap.h"
#include "Controllers/userinterface.h"

#include "Items/armor.h"
#include "Items/elixir.h"
#include "Items/potion.h"
#include "Items/weapon.h"

#include "Controllers/guardcontroller.h"
#include "Controllers/stationarycontroller.h"

Level::Level(const string &level_n) : _rows(), _columns()
{
    std::ifstream levelfile;
    levelfile.open(level_n.c_str());

    if (!levelfile.good()) {
         throw std::invalid_argument("File not found!");
      }

      vector<Node> nodes;
          while (true) {
              Node node;
              levelfile >> node;
              nodes.push_back(node);
              if (levelfile.eof())
                  break;
          }

          for (const auto &n : nodes) {
                 if(n.name == "Map Information"){
                     _rows = n.get<int>("rows");
                     _columns = n.get<int>("cols");
                     break;
                 }
             }


             _currentLevel = new Tile**[_rows];
             for(int x=0; x<_rows; x++){
                 _currentLevel[x] = new Tile*[_columns];
             }

//             vector<std::tuple<Item*, int, int>> items;
             vector<std::tuple<Tile*, int, int>> active;
             vector<std::tuple<Tile*, std::vector<int>, std::vector<int>>> switchDingsBums;
             Character* basic = nullptr;

             for (const auto &n : nodes) {
                 if (n.name == "Floor") {
                     _currentLevel[n.get<int>("row")][n.get<int>("col")] = new Floor(n.get<int>("row"), n.get<int>("col"));
                 }
                 if(n.name == "Wall"){
                     _currentLevel[n.get<int>("row")][n.get<int>("col")] = new Wall(n.get<int>("row"), n.get<int>("col"));
                 }
                 if(n.name == "Portal"){
                     _currentLevel[n.get<int>("row")][n.get<int>("col")] = new Portal(n.get<int>("row"), n.get<int>("col"));
                     active.push_back(std::make_tuple(_currentLevel[n.get<int>("row")][n.get<int>("col")], n.get<int>("destrow"), n.get<int>("destcol")));
                 }
                 if(n.name == "Switch"){
                     _currentLevel[n.get<int>("row")][n.get<int>("col")] = new Switch(n.get<int>("row"), n.get<int>("col"));
                     switchDingsBums.push_back(std::make_tuple(_currentLevel[n.get<int>("row")][ n.get<int>("col")], n.get<vector<int>>("destrows"), n.get<vector<int>>("destcols")));
                 }
                 if(n.name == "Lever"){
                     _currentLevel[n.get<int>("row")][n.get<int>("col")] = new Lever(n.get<int>("row"), n.get<int>("col"));
                     switchDingsBums.push_back(std::make_tuple(_currentLevel[n.get<int>("row")][ n.get<int>("col")], n.get<vector<int>>("destrows"), n.get<vector<int>>("destcols")));
                 }
                 if(n.name == "Door"){
                     _currentLevel[n.get<int>("row")][n.get<int>("col")] = new Door(n.get<int>("row"), n.get<int>("col"));
                 }
                 if(n.name == "Trap"){
                     _currentLevel[n.get<int>("row")][n.get<int>("col")] = new Trap(n.get<int>("row"), n.get<int>("col"));
                 }


//                 if(n.name == "Item"){
//                     if(n.get<string>("type") == "Weapon"){
//                        Item* w = new Weapon(n.get<string>("name"), n.get<int>("strbonus"));
//                        items.push_back(std::make_tuple(w, n.get<int>("row"), n.get<int>("col")));
//                     }
//                     else if(n.get<string>("type") == "Armor"){
//                         Item* a = new Armor(n.get<string>("name"), n.get<int>("stabonus"));
//                         items.push_back(std::make_tuple(a, n.get<int>("row"), n.get<int>("col")));
//                      }
//                     else if(n.get<string>("type") == "Potion"){
//                         Item* p = new Potion(n.get<string>("name"), n.get<int>("amount"), n.get<int>("hp"));
//                         items.push_back(std::make_tuple(p, n.get<int>("row"), n.get<int>("col")));
//                      }
//                     else if(n.get<string>("type") == "Elixir"){
//                         Item* e = new Elixir(n.get<string>("name"), n.get<int>("amount"), n.get<int>("hp"));
//                         items.push_back(std::make_tuple(e, n.get<int>("row"), n.get<int>("col")));
//                      }
//                 }


                 if(n.name == "Character"){
                     basic = new Character(n.get<char>("icon"), 20, 20);
                     placeCharacter(basic, n.get<int>("row"), n.get<int>("col"));
                     basic->setTile(_currentLevel[n.get<int>("row")][n.get<int>("col")]);

                     // here can be different types of Controllers, it doesn#t work
                     Controller* c = nullptr;
                        if(n.get<string>("controller") == "GuardController"){
                            c = new GuardController(n.get<string>("pattern"));
                            basic->setController(c);
                        }
                        if(n.get<string>("controller") == "StationaryController"){
                            c = new StationaryController();
                            basic->setController(c);
                        }

//                     if(n.get<string>("controller") == "ConsoleController"){
//                         c = new UserInterface();
//                     basic->setController(c);
//                     }
                 }
             }

             for(size_t i = 0; i < active.size(); i++){
                 dynamic_cast<Portal*>(_currentLevel[std::get<0>(active.at(i))->getRow()][std::get<0>(active.at(i))->getColumn()])->setDestination(dynamic_cast<Portal*>(_currentLevel[std::get<1>(active.at(i))][std::get<2>(active.at(i))]));
             }

// Schleife speziell für Switches or Levers. Wir gehen alle Tupels durch und fügen Doors zu einem Switch ein. Die Anzahl an den Doors ist von der Size der Vektoren in diesem Tuple abhängig
             for(size_t j = 0; j < switchDingsBums.size(); j++){
                 for(size_t Vsize = 0; Vsize < std::get<1>(switchDingsBums.at(j)).size(); Vsize++){
                     dynamic_cast<Active*>(_currentLevel[std::get<0>(switchDingsBums.at(j))->getRow()][std::get<0>(switchDingsBums.at(j))->getColumn()])
                             ->attach(dynamic_cast<Passive*>(_currentLevel[std::get<1>(switchDingsBums.at(j)).at(Vsize)][std::get<2>(switchDingsBums.at(j)).at(Vsize)]));
                 }
             }

//             for(size_t u = 0; u < items.size(); u++){
//                 _currentLevel[std::get<1>(items.at(u))][std::get<2>(active.at(u))]->setItem(std::get<0>(items.at(u)));
//             }
}



    Level::Level() : _rows(5), _columns(5)
    {
        _currentLevel = new Tile**[_rows];
        for(int x=0; x<_rows; x++){
            _currentLevel[x] = new Tile*[_columns];
        }

        Character* basic = nullptr;
        int x = 1;
        int y = 3;

        for (int i=0; i<_rows; i++){
            for(int j=0; j<_columns; j++){

                if((i==x && j==x) || (i==y && j==y)){
                    _currentLevel[i][j] = new Portal(i, j);
                }
                else if((i==0 && j==3) || (i==1 && j==3) || (i==2 && j==3)){
                    _currentLevel[i][j] = new Wall(i, j);
                }
                else if(i==4 && j==3){
                    _currentLevel[i][j] = new Door(i, j);
                }
                else if(i==0 && j==0){
                    _currentLevel[i][j] = new Switch(i, j);
                }
                else{
                    _currentLevel[i][j] = new Floor(i, j);
                }
            }
        }
        // portals
            dynamic_cast<Portal*>(_currentLevel[x][x])->setDestination(dynamic_cast<Portal*>(_currentLevel[y][y]));
            dynamic_cast<Portal*>(_currentLevel[y][y])->setDestination(dynamic_cast<Portal*>(_currentLevel[x][x]));

            dynamic_cast<Switch*>(_currentLevel[0][0])->attach(dynamic_cast<Door*>(_currentLevel[4][3]));

            basic = new Character('A', 20, 20);
            placeCharacter(basic, 0, 2);
            basic->setTile(_currentLevel[0][2]);
}

Level::~Level()
{
    for(int i=0; i<_rows; i++){
        for(int j=0; j<_columns; j++){
            delete _currentLevel[i][j];
        }
    }
    for(int x=0; x<_rows; x++){
        delete [] _currentLevel[x];
    }
    delete [] _currentLevel;
}

int Level::getRows(){ return _rows; }
int Level::getCols(){ return _columns; }

Tile* Level::getTile(int row_n, int col_n){
    return _currentLevel[row_n][col_n];
}

const Tile* Level::getTile(int row_n, int col_n) const{
    return _currentLevel[row_n][col_n];
}

void Level::placeCharacter(Character* c_n, int row_n, int col_n){
    _currentLevel[row_n][col_n]->setCharacter(c_n);
}
